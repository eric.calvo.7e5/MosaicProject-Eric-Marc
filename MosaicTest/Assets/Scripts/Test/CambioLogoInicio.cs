using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioLogoInicio : MonoBehaviour
{
    public GameObject logocambio;
    public GameObject square;
    public Camera c_main;
   
    float timeLoadScene = 0f;
    float timeChangeLogo = 0f;

    void Start()
    {
    
        logocambio = GameObject.Find("itb");
        square = GameObject.Find("Square");
       
        timeLoadScene =6;
        timeChangeLogo = 2;
    }

    // Update is called once per frame
    void Update()
    {
      
        //Cambio Escena
        if (timeLoadScene > 0)  timeLoadScene -= Time.deltaTime;
        else Application.LoadLevel("Menu");

        //Cambio Logo
        if (timeChangeLogo > 0)
        { 
            timeChangeLogo -= Time.deltaTime; 
        }
        else
        {
            DestroyObject(logocambio);
            DestroyObject(square);
            c_main.backgroundColor = Color.white;

        }
    }
}
