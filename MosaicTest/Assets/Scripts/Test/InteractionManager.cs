using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class InteractionManager : MonoBehaviour
{
    public SoundManager soundmanager;
    public MatrixBehaviour matrixBehaviour;
    public Camera cam;
    private int contador = 0;
    private float contadorAciertos = 0;
    public TMP_Text porcentaje;
    public GameObject advice;
    private int intentos = 2;
    public string MensajeTexto;

    public float auxcontadoracprotanopia;
    public float auxcontadoracdeutranopia;
    public float auxcontadoratritanopia;
    private float time;

    /*
    private void Awake()
    {
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 3:
                contadorAciertos = auxcontadoracprotanopia;
                PlayerPrefs.SetInt("auxcontadoracprotanopia", auxcontadoracprotanopia);

                break;

            case 4:
                contadorAciertos = auxcontadoracdeutranopia;
                PlayerPrefs.SetInt("auxcontadoracdeutranopia", auxcontadoracdeutranopia);

                break;

            case 5:
                contadorAciertos = auxcontadoratritanopia;
                PlayerPrefs.SetInt("auxcontadoratritanopia", auxcontadoratritanopia);

                break;
        }
        PlayerPrefs.Save();
    }*/

    private void Start()
    {
        time = 4f;
        cam = Camera.main;
        advice.SetActive(false);
    }

    private void Update()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "ProtanopiaTest":
                MensajeTexto = "Rojo Verde: ";
                break;

            case "DeuteranopiaTest":
                MensajeTexto = "Azul Morado: ";
                break;

            case "TritanopiaTest":
                MensajeTexto = "Verde Púrpura: ";
                break;
        }
        RayCastHit();
    }

    private void RayCastHit()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = -5f;
        mousePos = cam.ScreenToWorldPoint(mousePos);
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(mousePos, new Vector3(0, 0, 1f));

            if (hit.collider != null)
            {
                float _H, _S, _V;

                Color.RGBToHSV(hit.collider.gameObject.GetComponent<SpriteRenderer>().color, out _H, out _S, out _V);
                Debug.Log(_H);
                Debug.Log(matrixBehaviour.Hue);
                Debug.Log("suma : " + (_H - matrixBehaviour.Hue));
                if (Mathf.Abs(_H - matrixBehaviour.Hue) < 0.00001f)
                {
                    soundmanager.SeleccionAudio(0, 3f);
                    matrixBehaviour.MosaicGenerator();
                    matrixBehaviour.MosaicRandomizer();
                    //Compta de 5 en 5 per simular percentatge directament
                    contadorAciertos += 5;

                    porcentaje.GetComponent<TMP_Text>().text = MensajeTexto + contadorAciertos + "%";
                    matrixBehaviour.CambiarHue(contadorAciertos);
                    SetValues();
                    if (contadorAciertos == 100)
                    {
                        advice.SetActive(true);
                        Invoke("DesactivarAdvice", 90f);
                        advice.GetComponent<TMP_Text>().text = "¡Perfecto , has superado el test!"
                                                                      + "Pasa al siguiente Test con el boton de Next;";
                    }
                }
                else
                {
                    soundmanager.SeleccionAudio(1, 3f);
                    contador++;
                    intentos--;
                    if (contador == 2)
                    {
                        PlayerPrefs.SetFloat("score", contadorAciertos);
                        soundmanager.SeleccionAudio(1, 90f);
                        advice.GetComponent<TMP_Text>().text = "Te has quedado sin intentos :(. ¡Inténtalo otra vez!";
                        SetValues();
                        switch (SceneManager.GetActiveScene().buildIndex)
                        {
                            case 3:
                                SceneManager.LoadScene(4);
                                break;

                            case 4:
                                SceneManager.LoadScene(5);
                                break;

                            case 5:
                                SceneManager.LoadScene(6);
                                break;
                        }
                    }
                    if (contador == 1)
                    {
                        advice.SetActive(true);
                        Invoke("DesactivarAdvice", 3f);
                        advice.GetComponent<TMP_Text>().text = "Te queda un intento. Ten cuidado...";
                    }
                    else
                    {
                        soundmanager.SeleccionAudio(1, 90f);
                        advice.GetComponent<TMP_Text>().text = "Te has quedado sin intentos :(. ¡Inténtalo otra vez!";
                        SetValues();
                    }
                }
            }
        }
    }

    private void DesactivarAdvice()
    {
        advice.SetActive(false);
    }

    public void SetValues()
    {
        switch (SceneManager.GetActiveScene().buildIndex)
        {
            case 3:
                auxcontadoracprotanopia = contadorAciertos;
                PlayerPrefs.SetFloat("auxcontadoracprotanopia", auxcontadoracprotanopia);

                break;

            case 4:
                auxcontadoracdeutranopia = contadorAciertos;
                PlayerPrefs.SetFloat("auxcontadoracdeutranopia", auxcontadoracdeutranopia);

                break;

            case 5:
                auxcontadoratritanopia = contadorAciertos;
                PlayerPrefs.SetFloat("auxcontadoratritanopia", auxcontadoratritanopia);

                break;
        }
        PlayerPrefs.Save();
    }
}