using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatrixBehaviour : MonoBehaviour
{
    public GameObject prefab;
    private GameObject[,] arrayCuadrados;
    public Color littleMatrixColor;
    private int _randomNumberX;
    private int _randomNumberY;
    public float[,] arrayLuminiscencia = new float[18, 18];
    public Color colorInicial;
    public Color colorFinal;
    public float Hue;
    public float _Hcomponent;

    //Genera el mosaico 18x18
    public void MosaicGenerator()
    {
        arrayCuadrados = new GameObject[18, 18];
        for (int i = 0; i < arrayCuadrados.GetLength(0); i++)
        {
            for (int j = 0; j < arrayCuadrados.GetLength(1); j++)
            {
                arrayCuadrados[i, j] = Instantiate(prefab, transform.position + new Vector3(0.35f * i, 0.35f * j, 0), Quaternion.identity);
            }
        }
    }

    //Encargado de generar aleatoriamente un mosaico 3x3 dentro del de 18x18
    public void MosaicRandomizer()
    {
        _randomNumberX = Random.Range(0, 16);
        _randomNumberY = Random.Range(2, 18);

        for (int i = _randomNumberX; i <= _randomNumberX + 2; i++)
        {
            for (int j = _randomNumberY; j >= _randomNumberY - 2; j--)
            {
                arrayCuadrados[i, j].GetComponent<SpriteRenderer>().color = littleMatrixColor;
            }
        }
    }

    public void Start()
    {
        Debug.Log(_Hcomponent);
        //Aplicar los dos metodos anteriores + darle un brillo aleatorio a cada pieza del mosaico
        MosaicGenerator();
        MosaicRandomizer();
        for (int i = 0; i < arrayLuminiscencia.GetLength(0); i++)
        {
            for (int j = 0; j < arrayLuminiscencia.GetLength(1); j++)
            {
                arrayLuminiscencia[i, j] = Random.Range(.4f, .6f);
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
        //Cambia el componente value de HSV (= brillo) de cada cuadrado del mosaico en cada frame (ya que estamos en el update)
        for (int i = 0; i < arrayCuadrados.GetLength(0); i++)
        {
            for (int j = 0; j < arrayCuadrados.GetLength(1); j++)
            {
                arrayCuadrados[i, j].GetComponent<SpriteRenderer>().color = Color.HSVToRGB(_Hcomponent, .6f, arrayLuminiscencia[i, j] + Mathf.Sin((arrayLuminiscencia[i, j] * Time.frameCount + arrayLuminiscencia[i, j]) / 60f) * 0.2f);
            }
        }
        //Lo mismo que arriba pero en el mosaico 3x3
        for (int i = _randomNumberX; i <= _randomNumberX + 2; i++)
        {
            for (int j = _randomNumberY; j >= _randomNumberY - 2; j--)
            {
                littleMatrixColor = Color.HSVToRGB(Hue, .6f, arrayLuminiscencia[i, j] + Mathf.Sin((arrayLuminiscencia[i, j] * Time.frameCount + arrayLuminiscencia[i, j]) / 60f) * 0.2f);
                arrayCuadrados[i, j].GetComponent<SpriteRenderer>().color = littleMatrixColor;
            }
        }
    }

    public void CambiarHue(float contadorPorcentaje)
    {
        float _h;
        Color.RGBToHSV(Color.Lerp(colorInicial, colorFinal, contadorPorcentaje / 100f), out _h, out float s, out float v);
        Hue = _h;
    }
}