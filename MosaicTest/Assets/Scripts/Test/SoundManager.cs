using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] audios;

    public AudioSource controlAudio;

    private void Awake()
    {
    }

    public void SeleccionAudio(int indice , float volumen)
    {
        controlAudio.PlayOneShot(audios[indice], volumen);
    }
}
