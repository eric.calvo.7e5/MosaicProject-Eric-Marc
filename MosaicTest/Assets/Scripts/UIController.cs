using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public MatrixBehaviour _matrixBehaviour;

    public void LoadTest()
    {
        SceneManager.LoadScene("ProtanopiaTest");
    }

    public void LoadInstructions()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void ExitGame()
    {
        Application.Quit();
       
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void LoadResults()
    {
        SceneManager.LoadScene("Results");
    }

    public void LoadDeuteranopia()
    {
        SceneManager.LoadScene("DeuteranopiaTest");
    }

    public void LoadTritanopia()
    {
        SceneManager.LoadScene("TritanopiaTest");
    }

}